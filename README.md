# Front-End Utilities

Front-End utilities for Okulistik.

## Fake pagination
```
new utilities.fakePagination(data, itemsPerPage, paginationDestination, styleClasses, dataAttribute); 
```

### Usage

```
var pagination = new utilities.fakePagination(data, 12, '#pagination', 'waves-effect waves-light btn', 'data-page'); 
```

### Init 
```
pagination.init();
```
### Set pagination

```
pagination.setPagination();
```
### Change page

```
pagination.changePage(pageNumber);
```

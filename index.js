var utilities = (function () {
  var fakePagination = function (data, perPage, destination, classes, dataAttr) {
    this.data = data;
    this.totalCount = data.length;
    this.perPage = perPage;
    this.destination = destination;
    this.classes = classes;
    this.dataAttr = dataAttr;
  }

  fakePagination.prototype.init = function () {
    var data = this.changePage(1);
    if (this.totalCount > this.perPage)
      this.setPagination();
    return data;
  }

  fakePagination.prototype.setPagination = function () {
    var pageNumber = Math.ceil(this.totalCount / this.perPage);
    var html = '';
    for (var i = 1; i <= pageNumber; i++) {
      html += '<a class="' + this.classes + '" ' + this.dataAttr + '=' + i + '>' + i + '</a> '
    }
    document.querySelector(this.destination).innerHTML = html;
  }

  fakePagination.prototype.changePage = function (page) {
    var start = (--page) * this.perPage;
    var data = this.data.slice(start, start + this.perPage);
    return data;
  }

  return {
    fakePagination: fakePagination
  } 
})();
